<?php
ini_set('user_agent', 'Mozilla/5.0 php (compatible; Fursurv-Random-Images/1.0 +https://fur.feralfox.net/docs)');
$url = "https://fur.feralfox.net/v1"; // API url
$apikey = ""; // API key
$tagcat = "animals/fox"; // Tag and Category to interface with. Example: animals/fox (Will split into own variables soon!)
$string = file_get_contents("".$url."/?key=".$apikey."&tag=".$tagcat."");
$array = json_decode($string, true);
// *** these uncommented things are debug stuff, excuse that! ***
//echo "<pre>";
//var_dump($array);

//echo "</pre>";
//echo $array["success"]["item"]["image-url"];

//echo $array[""];

// Define recursive function to extract nested values
function printValues($arr) {
    global $count;
    global $values;

    // Check input is an array
    if(!is_array($arr)){
        die("ERROR: Input is not an array");
    }

    /*
    Loop through array, if value is itself an array recursively call the
    function else add the value found to the output items array,
    and increment counter by 1 for each value found
    */
    foreach($arr as $key=>$value){
        if(is_array($value)){
            printValues($value);
        } else{
            $values[] = $value;
            $count++;
        }
    }

    // Return total count and values found in array
    return array('total' => $count, 'values' => $values);
}


// Decode JSON data into PHP associative array format
$arr = json_decode($string, true);
// Set variable for the image url
$imageurl = $arr["item"]["image-url"];
?>
<html>
<head>
<style>
  img {
      width: auto;
      height: auto;
  }
</style>
<meta charset="utf-8">
<meta type="author" content="FeskVulpse" />
<meta type="description" content="Random Foxes! Powered by RFOS & FurSurv!" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="theme-color" content="#FF6600" />
<meta http-equiv="Cache-Control" content="max-age=1" />
<meta property="og:image" content="<?php echo $imageurl;?>" />
<meta property="og:title" content="RandomFox" />
<meta property="og:description" content="Random fox on every click!" />
<meta property="og:url" content="https://feralfox.net" />

</head>
<body>
<a href="<?php echo $imageurl;?>">Click here for the direct image</a><br>
<img src="<?php echo $imageurl;?>">
</body>
</html>

