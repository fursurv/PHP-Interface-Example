# PHP Interface Example
This is a very simple example of how to interface with the FurSurv API in PHP.

The API responds in json format.
[Live Demo Here](https://fur.feralfox.net/random/randomfox.php)

![Screenshot example](screenshot.png)
